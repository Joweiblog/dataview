import * as THREE from 'three'
import * as OrbitControls from 'three-orbitcontrols'
import TWEEN from '@tweenjs/tween.js'
import axios from 'axios'
import * as d3 from 'd3-geo'

export default class GeoMap {
  constructor(container) {
    this.container = container || window
    this.containerWidth = 0
    this.w_h = 1

    this.scene = null
    this.camera = null
    this.renderer = null
    this.controls = null
    this.mapGroup = [] // 组
    this.meshList = [] // 鼠标事件对象
    this.selectObject = null // 当前选中对象
    this.waveLoop = 0 // 循环标记
    this.cameraPath = null
  }

  init() {
    this.getCtRect()
    this.setScene()
    this.setCamera()
    this.setRenderer()
    this.setControl()
    this.getMap()
    this.animate()
    this.bindMouseEvent()
  }

  // 创建场景
  setScene() {
    this.scene = new THREE.Scene()
  }

  // 创建相机
  setCamera() {
    this.camera = new THREE.PerspectiveCamera(15, this.w_h, 1, 2000)
    this.camera.up.set(0, 0, 1)
    this.camera.lookAt(0, 0, 0)
    this.scene.add(this.camera)
  }

  // 创建渲染器
  setRenderer() {
    this.renderer = new THREE.WebGLRenderer({ antialias: true, alpha: true })
    this.renderer.setPixelRatio(window.devicePixelRatio * 1)
    this.renderer.sortObjects = true
    this.renderer.setClearColor(0x000000, 0)
    this.renderer.setSize(this.containerWidth, this.containerWidth / this.w_h)
    this.container.appendChild(this.renderer.domElement)

    function onWindowResize() {
      this.getCtRect()
      this.camera.aspect = this.w_h
      this.camera.updateProjectionMatrix()
      this.renderer.setSize(this.containerWidth, this.containerWidth / this.w_h)
    }

    window.addEventListener('resize', onWindowResize.bind(this), false)
  }

  // 控制器
  setControl() {
    this.controls = new OrbitControls(this.camera, this.renderer.domElement)
    // this.controls.autoRotate = false
    this.controls.maxAzimuthAngle = Math.PI * (3 / 4)
    this.controls.minAzimuthAngle = Math.PI * (1 / 3)
    this.controls.maxPolarAngle = Math.PI * (1 / 3)
    this.controls.minPolarAngle = 0
    this.controls.maxDistance = 500
    this.controls.minDistance = 150
  }

  // 容器宽高
  getCtRect() {
    if (this.container === window) {
      this.containerWidth = window.innerWidth
      this.w_h = window.innerWidth / window.innerHeight
    } else {
      const _rect = this.container.getBoundingClientRect() || {}
      this.containerWidth = _rect.width
      this.w_h = 1.5
    }
  }

  // 动画循环
  animate() {
    requestAnimationFrame(this.animate.bind(this))
    this.wave()
    this.moveCamera()
    TWEEN.update()
    this.controls.update()
    this.renderer.render(this.scene, this.camera)
  }

  // 移动相机
  moveCamera() {
    if (this.cameraPath === null) {
      // init path
      this.cameraPath = new THREE.Path()
      this.cameraPath.moveTo(0, 0)
      this.cameraPath.lineTo(150, 0)
      this.progress = 0
    } else {
      if (this.progress < 1) {
        this.progress += 0.025 // v
        let point = this.cameraPath.getPointAt(this.progress)
        if (point) {
          this.camera.position.set(point.x, point.y, 200)
        }
      }
    }
  }

  // 获取地图
  getMap() {
    axios.get('/china.json').then(res => {
      if (res.status !== 200) return
      this.setMapData(res.data)
    })
  }

  // 绘制地图 geojson
  setMapData(data) {
    let v3o = []
    data.features.forEach((features, featuresIndex) => {
      const areaItems = features.geometry.coordinates
      if (!features.properties.center) return
      features.properties.cp = this.lnglatToVector3(features.properties.center)
      v3o[featuresIndex] = {
        data: features.properties,
        mercator: [],
      }
      areaItems.forEach((item, areaIndex) => {
        v3o[featuresIndex].mercator[areaIndex] = []
        // todo: item[0] ? geojson 类型为 MultiPolygon 时，只绘制一层数据；
        item[0].forEach(ll => {
          const lnglat = this.lnglatToVector3(ll)
          const vector3 = new THREE.Vector3(
            lnglat[0],
            lnglat[1],
            lnglat[2]
          ).multiplyScalar(1)
          v3o[featuresIndex].mercator[areaIndex].push(vector3)
        })
      })
    })

    this.drawMap(v3o)
  }

  // 绘制图形 geojson
  drawMap(data) {
    this.mapGroup = new THREE.Group()
    this.mapGroup.position.y = 0
    this.scene.add(this.mapGroup)
    const extrudeSettings = {
      depth: 3,
      steps: 1,
      bevelSegments: 0,
      curveSegments: 1,
      bevelEnabled: false,
    }

    // mesh
    // back block
    const backBlockMaterial = new THREE.MeshBasicMaterial({
      color: '#283d61',
      opacity: 1,
      transparent: true,
    })
    // front block
    const frontBlockMaterial = new THREE.MeshBasicMaterial({
      color: '#314e6d',
      opacity: 0.7,
      transparent: true,
    })
    // line
    const lineMaterial = new THREE.LineBasicMaterial({
      color: '#a1b6cc',
    })

    data.forEach(areaData => {
      let areaGroup = new THREE.Group()
      areaGroup.name = 'area'
      areaGroup._groupType = 'areaBlock'

      areaData.mercator.forEach(areaItem => {
        // draw area
        let areaShape = new THREE.Shape(areaItem)
        let areaGeometry = new THREE.ExtrudeBufferGeometry(
          areaShape,
          extrudeSettings
        )
        let areaMesh = new THREE.Mesh(areaGeometry, [
          frontBlockMaterial,
          backBlockMaterial,
        ])
        areaGroup.add(areaMesh)

        // draw Line
        let lineGeometry = new THREE.Geometry()
        lineGeometry.vertices = areaItem
        let lineMesh = new THREE.Line(lineGeometry, lineMaterial)
        lineMesh.position.z = 3
        areaGroup.add(lineMesh)

        // add mesh to meshList for mouseEvent
        this.meshList.push(areaMesh)
      })

      areaGroup.add(this.lightBar(areaData))
      areaGroup.add(this.areaName(areaData))
      areaGroup.add(this.areaData(areaData))
      this.mapGroup.add(areaGroup)
    })

    this.scene.add(this.mapGroup)
  }

  // 坐标转换 lnglat [x,y]
  lnglatToVector3(lnglat) {
    if (!this.chinamap) {
      this.chinamap = d3
        .geoMercator()
        .center([107.770672, 32.559869])
        .scale(100)
        .translate([0, 0])
    }
    const [x, y] = this.chinamap([lnglat[0], lnglat[1]])
    return [y, x, 0]
  }

  // 鼠标hover事件
  bindMouseEvent() {
    let that = this

    function onMouseMove(event) {
      const x = (event.clientX / window.innerWidth) * 2 - 1 // 横坐标
      const y = -(event.clientY / window.innerHeight) * 2 + 1 // 纵坐标
      const standardVector = new THREE.Vector3(x, y, 0.5)
      // 设备坐标 => 世界坐标
      const worldVector = standardVector.unproject(that.camera)
      // 射线投射方向单位向量(worldVector坐标减相机位置坐标)
      const ray = worldVector.sub(that.camera.position).normalize()
      // 创建射线投射器对象
      let raycaster = new THREE.Raycaster(that.camera.position, ray)
      // 返回射线选中的对象
      let intersects = raycaster.intersectObjects(that.meshList)
      if (intersects.length) {
        if (
          intersects[0].object.parent &&
          intersects[0].object.parent._groupType === 'areaBlock'
        ) {
          if (that.selectObject !== intersects[0].object.parent) {
            if (that.selectObject) {
              transiform(
                that.selectObject.position,
                {
                  x: that.selectObject.position.x,
                  y: that.selectObject.position.y,
                  z: 0,
                },
                100
              )
              transiform(
                intersects[0].object.parent.position,
                {
                  x: intersects[0].object.parent.position.x,
                  y: intersects[0].object.parent.position.y,
                  z: 1.8,
                },
                100
              )
              that.selectObject = intersects[0].object.parent
            } else {
              transiform(
                intersects[0].object.parent.position,
                {
                  x: intersects[0].object.parent.position.x,
                  y: intersects[0].object.parent.position.y,
                  z: 1.8,
                },
                100
              )
              that.selectObject = intersects[0].object.parent
            }
          }
        }
      }
    }

    function transiform(o, n, t) {
      // eslint-disable-next-line no-unused-vars
      let e = new TWEEN.Tween(o).to(n, t).start()
    }

    window.addEventListener('mousemove', onMouseMove, false)
  }

  // lightBar
  lightBar(areaData) {
    let group = new THREE.Group()
    group.name = 'lightGroup'

    // bar
    let barGeometry = new THREE.CylinderGeometry(0.12, 0.12, 6, 64)
    let barMaterial = new THREE.MeshBasicMaterial({
      color: 0xcfeafe,
    })
    let barMesh = new THREE.Mesh(barGeometry, barMaterial)
    barMesh.rotation.x = Math.PI / 2
    barMesh.position.set(0, 0, 3.5)
    group.add(barMesh)

    // static circle
    let circleGeometry = new THREE.CircleBufferGeometry(0.8, 20)
    let circleMaterial = new THREE.MeshBasicMaterial({
      side: THREE.DoubleSide,
      blending: THREE.AdditiveBlending,
      color: '#ffffff',
      depthTest: false,
      transparent: true,
      opacity: 0.3,
    })
    let circleMesh = new THREE.Mesh(circleGeometry, circleMaterial)
    circleMesh.position.z = 0.1
    group.add(circleMesh)

    // wave circle
    let waveCircleGeometry = new THREE.CircleBufferGeometry(0.2, 20)
    let waveCircleMaterial = new THREE.MeshBasicMaterial({
      side: THREE.DoubleSide,
      blending: THREE.AdditiveBlending,
      color: 0xffffff,
      depthTest: false,
      transparent: true,
      opacity: 0.8,
    })
    let waveCircleMesh = new THREE.Mesh(waveCircleGeometry, waveCircleMaterial)
    waveCircleMesh.name = 'waveCircleMesh'
    waveCircleMesh.position.z = -0.995
    group.add(waveCircleMesh)

    // group setting
    group.position.x = areaData.data.cp[0]
    group.position.y = areaData.data.cp[1]
    group.position.z = 1.5
    group.rotation.z = Math.PI / 4
    group.renderOrder = 2

    return group
  }

  // areaName
  areaName(areaData) {
    let canvas = this.createTextSprite({
      width: 500,
      height: 60,
      fillStyle: 'rgb(119, 144, 172)',
      font: '40px Arial',
      text: areaData.data.name,
      textx: 250,
      texty: 40,
    })

    let texture = new THREE.CanvasTexture(canvas)
    texture.needsUpdate = true
    let SpriteMaterial = new THREE.SpriteMaterial({
      map: texture,
      depthTest: false,
    })

    let textSprite = new THREE.Sprite(SpriteMaterial)
    textSprite.position.set(areaData.data.cp[0], areaData.data.cp[1], 1)
    textSprite.scale.set(11, 1.2, 2.5)
    textSprite.renderOrder = 3

    return textSprite
  }

  // areaData
  areaData(areaData) {
    let canvas = this.createTextSprite({
      width: 400,
      height: 200,
      fillStyle: '#fff',
      bgStyle: 'rgba(5, 72, 146, 0.57)',
      font: '110px Arial',
      // todo: 绘制地图数据 text
      text: '11038',
      textx: 200,
      texty: 120,
    })

    let texture = new THREE.CanvasTexture(canvas)
    texture.needsUpdate = true
    let SpriteMaterial = new THREE.SpriteMaterial({
      map: texture,
      depthTest: false,
    })

    let textSprite = new THREE.Sprite(SpriteMaterial)
    textSprite.position.set(
      areaData.data.cp[0] - 1.1,
      areaData.data.cp[1] + 2.1,
      8
    )
    textSprite.scale.set(5, 2, 1)
    textSprite.renderOrder = 4

    return textSprite
  }

  // 波纹动画
  wave() {
    if (this.mapGroup.children) {
      if (this.waveLoop >= 1) {
        this.waveLoop = 0
      } else {
        this.waveLoop += 0.02
      }
      this.mapGroup.children.forEach(item => {
        if (item.name === 'area') {
          item.children.forEach(g => {
            if (g.name === 'lightGroup') {
              g.children.forEach(c => {
                if (c.name === 'waveCircleMesh') {
                  c.scale = {
                    x: 8 * Math.asin(this.waveLoop) + 1,
                    y: 8 * Math.asin(this.waveLoop) + 1,
                    z: 8 * Math.asin(this.waveLoop) + 1,
                  }
                  c.material.opacity = 0.3 * Math.acos(this.waveLoop) - 0.1
                }
              })
            }
          })
        }
      })
    }
  }

  // sprite
  createTextSprite({
    width = 100,
    height = 100,
    fillStyle = '#fff',
    bgStyle = '',
    font = '40px Arial',
    textAlign = 'center',
    text = '',
    textx = 0,
    texty = 0,
  }) {
    let canvas = document.createElement('canvas')
    canvas.width = width
    canvas.height = height
    canvas.style.position = 'absolute'
    canvas.style.top = 0
    canvas.style.left = 0
    canvas.style.zIndex = -999
    canvas.style.display = 'none'
    document.body.appendChild(canvas)

    let ctx = canvas.getContext('2d')

    if (bgStyle) {
      ctx.fillStyle = bgStyle
      ctx.fillRect(0, 0, width, height)
    }

    ctx.fillStyle = fillStyle
    ctx.font = font
    ctx.textAlign = textAlign
    ctx.fillText(text, textx, texty)

    return canvas
  }
}
