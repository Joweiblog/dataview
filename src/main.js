import '@babel/polyfill'

import Vue from 'vue'
import { Progress, Icon, Row, Col } from 'element-ui'
import 'normalize.css/normalize.css'
import 'element-ui/lib/theme-chalk/index.css'
import App from './App.vue'
import router from './router'

Vue.config.productionTip = false

Vue.use(Progress)
Vue.use(Icon)
Vue.use(Row)
Vue.use(Col)

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
