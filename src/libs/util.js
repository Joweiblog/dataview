const formatNumber = n => {
  const s = n.toString()
  return s[1] ? s : `0${s}`
}

// jClone
export function jClone(obj) {
  return JSON.parse(JSON.stringify(obj))
}

// 复制
export function clone(obj, hash = new WeakMap()) {
  if (!(typeof obj === 'object' && obj != null)) {
    return obj
  }

  if (hash.has(obj)) {
    return hash.get(obj)
  }

  // const target = Object.prototype.toString.call(obj) === '[object Array]' ? [] : {}
  const target = Array.isArray(obj) ? [] : {}
  hash.set(obj, target)

  let result = Object.keys(obj).map(key => ({
    [key]: clone(obj[key], hash),
  }))

  return Object.assign(target, ...result)
}

// 类型判断扩展
export function typeOf(o) {
  const toString = Object.prototype.toString
  const map = {
    '[object Boolean]': 'boolean',
    '[object Number]': 'number',
    '[object String]': 'string',
    '[object Function]': 'function',
    '[object Array]': 'array',
    '[object Date]': 'date',
    '[object RegExp]': 'regExp',
    '[object Undefined]': 'undefined',
    '[object Null]': 'null',
    '[object Object]': 'object',
    '[object Symbol]': 'symbol',
  }

  return map[toString.call(o)]
}

// throttle
export function throttle(fn, interval = 200) {
  let last
  let timer = null
  const time = interval
  return function(...args) {
    let that = this
    let now = +new Date()
    if (last && last - now < time) {
      clearTimeout(timer)
      timer = setTimeout(() => {
        last = now
        fn.apply(that, args)
      }, time)
    } else {
      last = now
      fn.apply(that, args)
    }
  }
}

// random
export function random(n = 1, m = 10) {
  return Math.round(Math.random() * (m - n) + n)
}

// format date
export function formatDate(date = new Date()) {
  let _date = date

  if (/^\d+$/.test(date)) {
    _date = parseInt(date)
  }

  if (['string', 'number'].includes(typeof _date)) {
    _date = new Date(_date)
  }

  if (!(_date.getFullYear && _date.getFullYear())) {
    throw new Error('日期格式错误')
  } else {
    const [Y, M, D, h, m, s] = [
      _date.getFullYear(),
      _date.getMonth() + 1,
      _date.getDate(),
      _date.getHours(),
      _date.getMinutes(),
      _date.getSeconds(),
    ]
    let d = `${[Y, M, D].map(formatNumber).join('.')}`
    let t = `${[h, m, s].map(formatNumber).join(':')}`
    return `${d} ${t}`
  }
}
