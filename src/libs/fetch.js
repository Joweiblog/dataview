import axios from 'axios'
import Q from 'q'

axios.defaults.timeout = 30000
axios.defaults.headers.post['Content-Type'] =
  'application/x-www-form-urlencoded'

export default function fetch({
  url = '',
  method = '',
  headers = {},
  data = {},
  ...options
}) {
  const ops = {
    method: method.toLocaleLowerCase(),
    url,
    headers,
    ...options,
  }

  if (ops.method === 'get') {
    ops.params = data
  } else {
    ops.data = data
  }

  try {
    return Q.resolve(axios(ops))
  } catch (e) {
    Q.reject(e)
  }
}
